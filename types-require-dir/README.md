# Installation
> `npm install --save @types/require-dir`

# Summary
This package contains type definitions for require-dir (https://github.com/aseemk/requireDir).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/require-dir.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/require-dir/index.d.ts)
````ts
// Type definitions for require-dir 1.0
// Project: https://github.com/aseemk/requireDir
// Definitions by: weekens <https://github.com/weekens>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

interface options {
    recurse?: boolean | undefined;
    duplicates?: boolean | undefined;
    extensions?: string[] | undefined;
    filter?: any;
    mapKey?: any;
    mapValue?: any;
}

declare function requireDir(directory: string, options?: options): { [path: string]: any };

export = requireDir;

````

### Additional Details
 * Last updated: Thu, 08 Jul 2021 22:42:00 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [weekens](https://github.com/weekens).
